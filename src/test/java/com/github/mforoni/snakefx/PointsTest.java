package com.github.mforoni.snakefx;

import static org.junit.jupiter.api.Assertions.*;

import com.github.mforoni.snakefx.Snake.Velocity;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

class PointsTest {

  @Test
  void random() {
    for (int maxX = 0; maxX <= 1000; maxX++) {
      for (int maxY = 0; maxY <= 1000; maxY++) {
        final Point point = Points.random(maxX, maxY);
        assertTrue(0 <= point.getX() && point.getX() <= maxX);
        assertTrue(0 <= point.getY() && point.getY() <= maxY);
      }
    }
  }

  @Test
  void random1() {
    for (int maxX = 0; maxX <= 1000; maxX++) {
      for (int maxY = 0; maxY <= 1000; maxY++) {
        final Point except = Points.random(maxX, maxY);
        final Point point = Points.random(maxX, maxY, ImmutableSet.of(except));
        if (point != null) {
          assertTrue(0 <= point.getX() && point.getX() <= maxX);
          assertTrue(0 <= point.getY() && point.getY() <= maxY);
          assertFalse(point.equals(except));
        }
      }
    }
  }

  @Test
  void translate() {
    final Point p = Points.random(100_000, 10_000);
    final Velocity v = new Velocity(12, 33);
    final Point t = Points.translate(p, v);
    assertTrue(t.getX() == p.getX() + v.getDx());
    assertTrue(t.getY() == p.getY() + v.getDy());
  }
}